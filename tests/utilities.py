import datetime
import lorem
import random
import time

from glapi import configuration

def synthetic_epics():
    """
    Generate epic data for testing.
    """

    result = list()

    x = {
        1: None,
        2: 1,
        3: 1,
        4: 3,
        5: 8,
        6: None,
        7: 4,
        8: None,
        9: None,
        10: 8
    }

    now = datetime.datetime.now()
    count = 10

    for (d,i) in enumerate(range(count)):
        time.sleep(1.35)
        date_start = now + datetime.timedelta(days=random.randint(0,400)) if datetime.datetime.now().second % 2 == 0 else now - datetime.timedelta(days=random.randint(0,400))
        date_end = date_start + datetime.timedelta(days=random.randint(0,400)) if datetime.datetime.now().second % 2 == 0 else None
        state = "opened" if datetime.datetime.now().second % 2 == 0 else "closed"
        result.append({
            "id": i + 1,
            "title": lorem.sentence()[0:-1],
            "description": lorem.paragraph(),
            "state": state,
            "end_date": date_end.strftime(configuration.DATE_ISO_8601) if date_end else None,
            "start_date": date_start.strftime(configuration.DATE_ISO_8601),
            "parent_id": x[i + 1],
            "web_url": configuration.GITLAB_API_VERSION.split("/api")[0],
            "group_id": random.randint(0,100),
            "iid": i,
            "author": { "id": random.randint(1, 10)}
        })

    return result
